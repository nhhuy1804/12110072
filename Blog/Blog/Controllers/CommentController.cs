﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Models;

namespace Blog.Controllers
{
    public class CommentController : Controller
    {
<<<<<<< HEAD
<<<<<<< HEAD
        private BlogDbContext db = new BlogDbContext();
=======
        private BlogDBContext db = new BlogDBContext();
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
=======
        private BlogDBContext db = new BlogDBContext();
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac

        //
        // GET: /Comment/

        public ActionResult Index()
        {
<<<<<<< HEAD
<<<<<<< HEAD
            var comments = db.Comments.Include(c => c.post);
            return View(comments.ToList());
=======
            return View(db.Comments.ToList());
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
=======
            return View(db.Comments.ToList());
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
        }

        //
        // GET: /Comment/Details/5

        public ActionResult Details(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // GET: /Comment/Create

        public ActionResult Create()
        {
<<<<<<< HEAD
<<<<<<< HEAD
            ViewBag.PostID = new SelectList(db.Posts, "ID", "Title");
=======
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
=======
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
            return View();
        }

        //
        // POST: /Comment/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
<<<<<<< HEAD
<<<<<<< HEAD
        public ActionResult Create(Comment comment, int idpost)
        {
            if (ModelState.IsValid)
            {
                comment.PostID = idpost;
                comment.DateCreate = DateTime.Now;
                db.Comments.Add(comment);
                db.SaveChanges();
                return RedirectToAction("Details/" + idpost, "Post");
            }

            ViewBag.PostID = new SelectList(db.Posts, "ID", "Title", comment.PostID);
=======
=======
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
        public ActionResult Create(Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

<<<<<<< HEAD
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
=======
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
            return View(comment);
        }

        //
        // GET: /Comment/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
<<<<<<< HEAD
<<<<<<< HEAD
            ViewBag.PostID = new SelectList(db.Posts, "ID", "Title", comment.PostID);
=======
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
=======
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
            return View(comment);
        }

        //
        // POST: /Comment/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
<<<<<<< HEAD
<<<<<<< HEAD
            ViewBag.PostID = new SelectList(db.Posts, "ID", "Title", comment.PostID);
=======
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
=======
>>>>>>> a1665d82f5ad3653f7b823bca70daa3f4d08a3ac
            return View(comment);
        }

        //
        // GET: /Comment/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // POST: /Comment/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}